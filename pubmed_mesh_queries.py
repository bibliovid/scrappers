import json, sys
from pymed import PubMed
from datetime import datetime, date
import time
import collections

pubmed = PubMed(tool="https://covid19.lis-lab.fr", email="benoit.favre@univ-amu.fr")

base_query ='"COVID-19" OR Coronavirus OR "Corona virus" OR "2019-nCoV" OR "SARS-CoV" OR "MERS-CoV" OR "Severe Acute Respiratory Syndrome" OR "Middle East Respiratory Syndrome"'
#query ='"COVID-19"'
#today = datetime.now().isoformat().split('T')[0]
#query = '(("%s"[Date - Publication] : "%s"[Date - Publication])) AND COVID-19[Text Word]' % (today, today)

count = collections.defaultdict(int)
seen = {}
data = []

for keyword in [ 'Diagnostic', 'Therapeutics', 'Epidemiology', 'Prognosis', 'Recommendations', 'Modeling', 'Hepato-gastroenterology', 'Neurology', 'Cardiology', 'Hematology', 'Geriatrics', 'Infectiology', 'Obstetric gynecology', 'Dermatology', 'Paediatrics', 'Pulmonology', 'Psychiatry', 'Virology', 'Anesthesics', 'Radiology', 'Hygiene', 'Nephrology', 'Lockdown', 'Immunity' ]:
  query = '"%s"[MeSH] AND (%s)' % (keyword, base_query)

  results = pubmed.query(query, max_results=10000)
  count[keyword] = 0

  for result in results:
    entry = result.toDict()
    pmid = entry['pubmed_id'].split('\n')[0]
    entry['pmid'] = entry['pubmid_id'] = pmid
    if pmid not in seen:
      seen[pmid] = len(data)
      entry['url'] = 'https://www.ncbi.nlm.nih.gov/pubmed/' + pmid
      if 'authors' in entry:
        entry['authors'] = '; '.join(['%s, %s' % (x['lastname'], x['firstname']) for x in entry['authors']])
      if 'xml' in entry:
        del entry['xml']
      for key, value in entry.items():
        if type(value) in [datetime, date]:
          entry[key] = value.isoformat()
      entry['mesh_query'] = []
      data.append(entry)
    data[seen[pmid]]['mesh_query'].append(keyword)
    count[keyword] += 1
  time.sleep(1)
  #print(data)

#print(len(data), file=sys.stderr)
for keyword, value in count.items():
  print(value, keyword, file=sys.stderr)
print(json.dumps(data, indent=2))

