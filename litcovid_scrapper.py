# Download litcovid json data from django API. 
# Unfortunately, we have to do it page by page.

import urllib.request, json, sys

url = 'https://www.ncbi.nlm.nih.gov/research/coronavirus-api/search/'
with urllib.request.urlopen(url) as response:
  data = json.loads(response.read())
  num_pages = data['total_pages']  

results = []
for page in range(num_pages):
  print(url + '?page=%d' % page, file=sys.stderr)
  with urllib.request.urlopen(url + '?page=%d' % (1 + page)) as response:
    data = json.loads(response.read())
  results.extend(data['results'])

print(json.dumps(results, indent=2))
