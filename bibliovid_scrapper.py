# Add article syntheses to bibliovid dump using the slug field.
# Note that we are very dependent on the html structure of the site.

import urllib.request, json, sys
from bs4 import BeautifulSoup
import bs4

with open(sys.argv[1]) as fp:
  articles = json.loads(fp.read())

for article in articles['results']:
  url = 'https://bibliovid.org/' + article['slug'] + '-' + str(article['id'])
  print(url, file=sys.stderr)
  with urllib.request.urlopen(url) as response:
    data = response.read()
    html = BeautifulSoup(data, 'html.parser')
    main = html.find(class_='bg-white rounded-lg p-2 md:p-6')
    divs = main.contents 
    #for i, div in enumerate(divs):
    #  print('%d [%s]' % (i, div))
    
    def safe_text(node):
      if type(node) is bs4.element.Tag:
        return node.get_text().strip()
      return ''

    #title = safe_text(main.find('h1'))
    #link = divs[8].find('a').attrs['href']
    #findings = safe_text(divs[12].find('div'))
    #take_away = safe_text(divs[14].contents[0].find('div'))
    #relevance_level = safe_text(divs[16].contents[0].find('div'))
    #objectives = safe_text(divs[18].contents[0].find('div'))
    #methods = safe_text(divs[20].contents[0].find('div'))

    #article['link'] = link
    #article['findings'] = findings
    #article['take_away'] = take_away
    #article['relevance_level'] = relevance_level
    #article['objectives'] = objectives
    #article['methods'] = methods

    title = safe_text(main.find('h1'))
    link = divs[8].find('a').attrs['href']
    findings = safe_text(divs[14].find('div'))
    take_away = safe_text(divs[16].contents[0].find('div'))
    relevance_level = safe_text(divs[18].contents[0].find('div'))
    objectives = safe_text(divs[20].contents[0].find('div'))
    methods = safe_text(divs[22].contents[0].find('div'))

    article['link'] = link
    article['results'] = findings
    article['synthesis'] = take_away
    article['strength_of_evidence_details'] = relevance_level
    article['goals'] = objectives
    article['methods'] = methods

print(json.dumps(articles, indent=2))

