import sys, json

with open(sys.argv[1]) as fp:
  articles = json.loads(fp.read())

if type(articles) == dict:
  articles = articles['results']

for article in articles:
  article['topics'] = [article['category']['name']] + [x['name'] for x in article['specialties']]
  article['author_list'] = article['authors']
  article['authors'] = ', '.join([x['name'] for x in article['authors']])
  day, month, year = article['verbose_date'].split('.')
  article['publication_date'] = '%s-%s-%s' % (year, month, day)

print(json.dumps(articles, indent=2))

