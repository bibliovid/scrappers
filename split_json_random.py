import json
import sys
import random

if len(sys.argv) != 5:
  print('usage: %s <output-stem> <n-folds> <test-percent> <valid-percent>' % sys.argv[0])
  sys.exit(1)

output_stem = sys.argv[1]
num_folds = int(sys.argv[2])

items = json.loads(sys.stdin.read())

num_test = int(float(sys.argv[3]) * len(items))
num_valid = int(float(sys.argv[4]) * len(items))

random.seed(12345)

for n in range(num_folds):
  random.shuffle(items)

  with open(output_stem + '-%d.test' % n, 'w') as fp:
    fp.write(json.dumps(items[:num_test]))

  with open(output_stem + '-%d.valid' % n, 'w') as fp:
    fp.write(json.dumps(items[num_test: num_test + num_valid]))

  with open(output_stem + '-%d.train' % n, 'w') as fp:
    fp.write(json.dumps(items[num_test + num_valid:]))
