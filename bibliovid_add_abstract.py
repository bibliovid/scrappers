# Download abstracts from pubmed when title/authors can be found.
# Note that we might get an incorrect match.

import sys, json, re, collections

from pymed import PubMed
from datetime import datetime, date
import requests
import time

pubmed = PubMed(tool="https://covid19.lis-lab.fr", email="benoit.favre@univ-amu.fr")

if len(sys.argv) != 3:
  print('usage: %s <input> <output>' % sys.argv[0], file=sys.stderr)
  sys.exit(1)

with open(sys.argv[1]) as fp:
  articles = json.loads(fp.read())

print(len(articles['results']), file=sys.stderr)

def normalize(text):
  return re.sub('[^a-zA-Z]', '', text).lower()

def preprocess(term):
  return re.sub(r'[()\[\]]', ' ', term)

stats = collections.defaultdict(int)

for article in articles['results']:
  title = article['title']
  authors = ' '.join(x['name'] for x in article['authors'])
  journal = article['journal']

  found = False
  for query in ['(%s[Title] AND (%s[Author])' % (preprocess(title), preprocess(authors)), '%s[Title]' % preprocess(title), preprocess(title)]:
    for retries in range(10):
      try:
        results = pubmed.query(query, max_results=30)
        for result in results:
          entry = result.toDict()
          if normalize(title) == normalize(entry['title']):
            found = True
            for field in ['pubmed_id', 'doi', 'abstract']:
              if field in entry:
                article[field] = entry[field]
                stats[field] += 1
            break
        break
      except requests.exceptions.HTTPError as e:
        print(e, file=sys.stderr)
        time.sleep(5)
    if found:
      break
  if not found:
    print('NOT FOUND:', title, file=sys.stderr)

with open(sys.argv[2], 'w') as fp:
  fp.write(json.dumps(articles, indent=2))

print('TOTAL', len(articles['results']), file=sys.stderr)
for key, value in stats.items():
  print(key, value, value / len(articles['results']), file=sys.stderr)



